require.config({
    baseUrl: 'app',
    paths: {
        'jquery': '../assets/js/jquery-2.1.1.min',
        'backbone': '../assets/js/backbone-min',
        'text': '../assets/js/require.text',
        'underscore': '../assets/js/underscore-min',
        'moment': '../assets/js/moment.min'
    },
    shim: {
        'jquery': {
            exports: '$'
        },
        'underscore': {
            exports: '_'
        },
        'backbone': {
            deps: ['underscore', 'jquery'],
            exports: 'Backbone'
        },
        'moment': {
            exports: 'moment'
        }
    }
});

require([
    'jquery',
    'backbone',
    'router',
    'config',
    'base/storage',
    'models/user'
    ], function($, Backbone, Router, config, storage, UserModel) {
        if(storage.get('apiToken')) {
            $.ajaxPrefilter(function(options) {
                options.url = options.url + ((options.url.indexOf('?') === -1) ? '?' : '&') + 'api_token=' + encodeURIComponent(storage.get('apiToken'));
            });
            var router = new Router();
            window.app = {};
            window.app.router = router;
            (new UserModel({
                id: 'self'
            })).fetch({
                success: function(user) {
                    window.app.user = user;
                    Backbone.history.start({
                        root: config.rootPath
                    });
                }
            });
        } else {
            require([
                'views/login'
                ], function(View)  {
                    var view = new View();
                    view.render();
                });
        }
        console.log();
    });
