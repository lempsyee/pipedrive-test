define([
    'base/model',
    'config'
], function(Model, config) {
    return Model.extend({
        urlRoot: config.api + '/deals'
    });
});