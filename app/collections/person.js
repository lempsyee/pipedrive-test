define([
    'base/collection',
    'models/person',
    'config'
    ], function(Collection, Model, config) {
        return Collection.extend({
            url: config.api + '/persons',
            model: Model
        });
    });