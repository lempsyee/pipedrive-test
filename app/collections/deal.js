define([
    'base/collection',
    'models/deal',
    'config'
    ], function(Collection, Model, config) {
        return Collection.extend({
            url: config.api + '/deals',
            model: Model
        });
    });