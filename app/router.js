define([
    'backbone',
    ], function(Backbone) {
        var rendered;
        return Backbone.Router.extend({
            routes: {
                "person/:person": "person",
                "person": "person",
                "": "person"
            },
            person: function(person) {
                require([
                    'views/person'
                    ], function(View) {
                        //because of the simplicity of this task i'm not rerendering the whole layout if the previous one is the same. will only rerender the person.data part later
                        if(rendered instanceof View) {
                            rendered.select(person);
                        } else {
                            var view = new View();
                            view.render();
                            if(person) {
                                view.on('render', function() {
                                    this.select(person);
                                });
                            }
                            rendered = view;
                        }
                    });
            }
        });
    });
