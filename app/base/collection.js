define([
    'backbone'
    ], function(Backbone) {
        return Backbone.Collection.extend({
            parse: function(response) {
                if ('success' in response) {
                    if (response.success) {
                        return response.data;
                    } else {
                        return false;
                    }
                } else {
                    return response;
                }
            }
        });
    })


