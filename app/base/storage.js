define([
    ], function() {
        var data = {};
        return {
            set: function(k, v) {
                if(typeof(Storage) !== "undefined") {
                    localStorage.setItem(k, v);
                } else {
                    data[k] = v;
                }
                return this;
            },
            get: function(k) {
                if(typeof(Storage) !== "undefined") {
                    return localStorage.getItem(k);
                } else {
                    return data[k] || null;
                }
            }
        }
    })