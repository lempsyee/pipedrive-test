define([
    'backbone',
    'underscore',
    'text!templates/person.html',
    'collections/person',
    'views/person.data',
    ], function(Backbone, _, template, PersonCollection, PersonDataView) {
        return Backbone.View.extend({
            template: _.template(template),
            events: {
                'click #sidebar nav a': function(e) {
                    window.app.router.navigate($(e.currentTarget).attr('href'), {
                        trigger: true
                    });
                }
            },
            persons: null,
            render: function() {
                var self = this;
                var c = new PersonCollection();
                c.fetch({
                    success: function() {
                        self.persons = c;
                        self.$el.html(self.template({
                            persons:c,
                            user: window.app.user
                        }));
                        $('#application').html(self.$el);
                        self.trigger('render');
                    }
                });
            },
            select: function(id) {
                if(this.persons) {
                    var found;
                    this.persons.each(function(person) {
                        if(person.get('id') === parseInt(id)) {
                            found = person;
                            return false;
                        }
                    });
                    if(found) {
                        this.$el.find('#sidebar nav a').removeClass('active').filter('[data-id="' + found.get('id') + '"]').addClass('active');
                        var view = new PersonDataView();
                        view.render(found);
                    }
                }
            }
        });
    });