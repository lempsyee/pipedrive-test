define([
    'backbone',
    'underscore',
    'text!templates/login.html',
    'config',
    'base/storage'
    ], function(Backbone, _, template, config, storage) {
        return Backbone.View.extend({
            template: _.template(template),
            events: {
                'submit form': function(e) {
                    var form = $(e.currentTarget);
                    e.preventDefault();
                    $.ajax({
                        url: config.api + '/authorizations',
                        method: 'post',
                        data: {
                            email: form.find('input[name="email"]').val(),
                            password: form.find('input[name="password"]').val()
                        },
                        success: function(j) {
                            storage.set('apiToken', j.data[0].api_token);
                            location.reload();
                        },
                        error: function(xhr) {
                            alert(xhr.responseJSON.error);
                        }
                    });
                }
            },
            persons: null,
            render: function() {
                this.$el.html(this.template());
                $('#application').html(this.$el);
            }
        });
    });