define([
    'backbone',
    'underscore',
    'moment',
    'text!templates/person.data.html',
    'collections/deal',
    'config'
    ], function(Backbone, _, moment, template, DealCollection, config) {
        return Backbone.View.extend({
            template: _.template(template),
            className: 'box cols cols-2 cols-collapsible',
            render: function(person) {
                var self = this, c = new (DealCollection.extend({
                    //override deal collection for a person specific deals list.
                    url: config.api + '/persons/'+person.id+'/deals'
                }));
                c.fetch({
                    success: function() {
                        self.$el.html(self.template({
                            moment:moment,
                            person:person,
                            deals:c
                        }));
                        $('#main').html(self.$el);
                    }
                })
            }
        });
    });